# -*- coding: utf-8 -*-
#=======================================================================================
#This File is part of FiPyRT
#=======================================================================================
#
#Copyright (C) 2016-2017  <Author> Ravi A. Patel <Email> ravee.a.patel@gmail.com
#
#This program is free software: you can redistribute it and/or modify it under the
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
#PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

#=======================================================================================
#%%
from __future__ import print_function,division
import fipy
from copy import deepcopy
from _phrqc_wrapper import  Phrqc as _Phrqc
from _gems_wrapper import xGems as _xGems
def Grid1D(l,dx):
       mesh = fipy.Grid1D(dx=dx,Lx=l,nx=l/dx)
       mesh.rank = mesh.communicator.procID
#       print ("1D mesh created. Extents of mesh are %s and \
#               number of elements equal to %s "%(mesh.extents,mesh.numberOfCells))
       return mesh
       
class Diffusion1D(object):
    _params=['phiw','c','D0','tort','solutiontype']
    _cellvars=['phiw','c','tort']
    _defaults=[1., 0., 0.5, 1., 1., 'crank']
    def __init__(self,mesh,dp={},bc={},solverparams={},component='c'):
        params,defaults = self._params,self._defaults
        inputs = deepcopy(dp)
        inputs.update(solverparams)
        for p,d in zip(params,defaults):
            setattr(self,p,inputs.get(p,d))
        for v in self._cellvars:
            setattr(self,v,fipy.CellVariable(name=v,mesh=mesh,value = 
            getattr(self,v)))
        for d in ['left','right']:
            v = bc.get(d,['flux',0])
            if v[0] =='flux':
                if d=='left': 
                    self.c.faceGrad.constrain(v[1],mesh.facesLeft)
                elif d=='right':
                    self.c.faceGrad.constrain(v[1],mesh.facesRight)
            if v[0]=='c':
                if d=='left': 
                    self.c.constrain(v[1],mesh.facesLeft)
                elif d=='right':
                    self.c.constrain(v[1],mesh.facesRight)
            if v[0] =='cauchy':
                if d=='left': 
                    self.c.faceGrad.constrain((self.c.faceValue*self.u.faceValue -
                    v[1]*self.u.faceValue)/self.De.faceValue,mesh.facesLeft)
                elif d=='right':
                    self.c.faceGrad.constrain((self.c.faceValue*self.u.faceValue -
                    v[1]*self.u.faceValue)/self.De.faceValue,mesh.facesRight)
        self.seteq()
        self.t = 0
        
    def seteq(self):
        self.c.eeq= fipy.TransientTerm(coeff=self.phiw) == fipy.ExplicitDiffusionTerm(coeff=self.De) 
        self.c.ieq= fipy.TransientTerm(coeff=self.phiw) == fipy.DiffusionTerm(coeff=self.De) 
        self.c.ceq= (self.c.eeq + self.c.ieq)
        
    @property
    def De(self):
        return self.phiw * self.tort * self.D0
        
    def advance(self,dt):
        self.t+=dt               
        if self.solutiontype == 'implicit':
            self.c.ieq.solve(var=self.c,dt=dt)   
        if self.solutiontype == 'explicit':
            self.c.eeq.solve(var=self.c,dt=dt)   
        if self.solutiontype == 'crank':
            self.c.ceq.solve(var=self.c,dt=dt)    

class AdvectionDiffusion1D(Diffusion1D):
    _params=['phiw','c','D0','tort','solutiontype','u']
    _defaults=[1., 0., 0.5, 1., 1., 'explicit',(0.,)]
    _cellvars=['phiw','c','tort','u']
    def seteq(self):
        self.c.eeq= fipy.TransientTerm(coeff=self.phiw) + fipy.ExplicitUpwindConvectionTerm(coeff=self.u) \
                    == fipy.ExplicitDiffusionTerm(coeff=self.De) 
        self.c.ieq= fipy.TransientTerm(coeff=self.phiw) + fipy.UpwindConvectionTerm(coeff=self.u) \
                    == fipy.DiffusionTerm(coeff=self.De) 
        self.c.ceq=  self.c.eeq + self.c.ieq
    
class MultiComponent(dict):

    def __init__(self,trans_model,mesh,components=[],dp={},bc={},solverparams={}):
        """
        set diffusion equation
        """
        self.components = components
        for name in self.components:
            localdp=deepcopy(dp)
            localbc=deepcopy(bc)
            if name in localdp:
                localdp.update(localdp[name])
            if name in localbc:
                localbc.update(localbc[name])
            self[name]=trans_model(mesh,localdp,localbc,solverparams,name)
            
    def cdict(self):
        out = {}
        for name in self.components:
            out[name]=self[name].c.value
        return out
    
    def advance(self,dt):
        for name in self.components:
            self[name].advance(dt)
            
    def update_conc(self,c):
        for name in self.components:
            self[name].c.setValue(c[name])

class SolidConc(dict):
    def __init__(self,mesh,cdict):
        for k,v in cdict.iteritems():
            self[k]=fipy.CellVariable(name=k,mesh=mesh,value=v)
    def update(self,cdict):
        for k,v in cdict.iteritems():
            self[k].setValue(value=v)        
        
class PhrqcReactiveTransport1D(object):
    """
    Phreeqc Reactive transport model
    """
    def __init__(self,trans_model,mesh,dp={},bc={},solverparams={}):
        ncells = mesh.numberOfCells
        inputs = deepcopy(dp)
        inputs.update(bc)
        inputs.update(solverparams)
        self.phrqc=_Phrqc(ncells,dp,bc,solverparams)
        components = self.phrqc.components
        bc.update(self._getBCFromPhrqc(bc,self.phrqc.boundary_conditions())) 
        dp.update(self._getICFromPhrqc(self.phrqc.initial_conditions()))
        self.trans = MultiComponent(trans_model,mesh,components,
                                        dp,bc,solverparams)
        self.solidconc = SolidConc(mesh,self.phrqc.solid_phase_conc())
        self.t = 0
    def advance(self,dt):
        """
        advance a timestep
        """
        self.t += dt
        self.trans.advance(dt)
        c = self.trans.cdict()
        c = self.phrqc.advance(c,dt)
        self.trans.update_conc(c)
        self.solidconc.update(self.phrqc.solid_phase_conc())
    @staticmethod
    def _getICFromPhrqc(c):
        out ={}
        for name in c.keys():
            out[name]={}
            out[name]['c']=c[name]
        return out
    
    @staticmethod
    def _getBCFromPhrqc(bc,pbc):
        out ={}
        for name in pbc.keys():
            for k in pbc[name].keys():
                out[k]={}
                out[k][name]=[bc[name][0],pbc[name][k]]
        return out

class GemsReactiveTransport1D(object):
    """
    GEMS Reactive transport model
    """
    def __init__(self,trans_model,mesh,dp={},bc={},solverparams={}):
        ncells = mesh.numberOfCells
        inputs = deepcopy(dp)
        inputs.update(bc)
        inputs.update(solverparams)
        self.gem=_xGems(ncells,dp,bc)
        elements = self.gem.element_names
        dp.update(self._getICFromGems())
        bc.update(self._getBCFromGems(bc)) 
        self.trans = MultiComponent(trans_model,mesh,elements,
                                        dp,bc,solverparams)
        self.t = 0
    def advance(self,dt):
        """
        advance a timestep
        """
        self.t += dt
        self.trans.advance(dt)
        c = self.trans.cdict()
        self.gem.advance(c)
        c = self.gem.get_cdict()
        self.trans.update_conc(c)
        
    def _getICFromGems(self):
        out ={}
        c=self.gem.get_ic()
        for name in self.gem.element_names:
            out[name]={}
            out[name]['c']=c[name]
        return out

    def _getBCFromGems(self,bc):
        gbc = self.gem.get_bc()
        out ={}
        soln_labels = bc['boundary_solution_labels']
        for name in self.gem.element_names:
            out[name]={}
            for side in ['left','right']:
                if side in soln_labels:
                    idx = soln_labels[side]
                    out[name][side]=[bc[side][0],gbc[idx][name]]
                else:
                    out[name][side]=[bc[side][0],bc[side][1]]
        return out