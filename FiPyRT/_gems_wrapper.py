# -*- coding: utf-8 -*-
#=======================================================================================
#This File is part of FiPyRT
#=======================================================================================
#
#Copyright (C) 2016-2017  <Author> Ravi A. Patel <Email> ravee.a.patel@gmail.com
#
#This program is free software: you can redistribute it and/or modify it under the
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
#PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

#=======================================================================================
#%%
from __future__ import division,print_function
import sys
from xgems import ChemicalEngine 
import numpy as np
from copy import deepcopy as copy
#%%
class VarDef(object):
    """
    a class to define variable used for convenience 
    """
    _args_name=['type','default','doc']
    def __init__(self,*args):
        for name,val in zip(self._args_name,args):
            setattr(self,name,val)
#%%
class xGems(object):
    """
    xGems object for coupling with reactive transport
    """
    _vars={'gems_init_file':VarDef('str','','Gems file for initialization ends with .lst'),
           'gems_ic_files':VarDef('list',[],'Gems dbr files list for setting initial conditions'),
           'ic_cell_labels':VarDef('scalar',0,'an array specify cell label corresponding to initial state given by dbr file'),
           'gems_bc_files':VarDef('list',[],'Gems dbr files for setting boundary conditions'),
           'T':VarDef('scalar',0,'temprature for each cell'),
           'pH':VarDef('scalar',0,'pH'),
           'pe':VarDef('scalar',0,'pe'),
           'P':VarDef('scalar',0,'pressure for each cell'),
           'nx':VarDef('param',0,'number of cells'),
            '_poros':VarDef('scalar',1,'internal variable for porosity'),
            '_phaseVolFrac':VarDef('dict',1,'internal for  phase volume fractions'),
            '_phaseConc':VarDef('dict',1,'internal for  phase concentration'),
            'pH':VarDef('scalar',0,'pH'),
            'pE':VarDef('scalar',0,'pe'),
            }
    
    def __init__(self,ncells,dp={},bc={}):
        """
        initalizes gems reaction module
        
        Input
        -----
         inputs: dict
             dictionary containing inputs for initializing xGems classs
             inputs['gems_init_file']: input file obtained from GEMS3K to initialize xGems
             inputs['gems_ic_files']: list containing files obtained from GEMS3K for initial conditions
             inputs['gems_bc_files']:  list containing files obtained from GEMS3K for boundary conditions
             inputs['ic_cell_labels']: array to specify label for cell corresponding to index of files in initial conditions
        """
        self.nx = ncells  
        inputs = copy(dp)
        inputs.update(bc)
        self._read_inputs(inputs)  
        self.gem = ChemicalEngine(self.gems_init_file) 
        self.nelements = self.gem.numElements()
        self.element_names = []
        for i in range(self.nelements):
            self.element_names.append(self.gem.elementName(i))
        self.nphases= self.gem.numPhases()
        self.phase_names =[]
        for i in range(self.nphases):
            self.phase_names.append(self.gem.phaseName(i))
        
    def _read_inputs(self,inputs):
        """
        a convenience method to read inputs
        """
        skip_items=['nx']
        for k,v in self._vars.iteritems():
            if k not in skip_items:
                if v.type=='scalar':
                    setattr(self,k,inputs.get(k,v.default)*np.array([1]*self.nx))
                else:
                    setattr(self,k,inputs.get(k,v.default))
            
    def advance(self,c_dict):
        """
        advances a timestep in calculation
        
        Input
        -----
        c_dict: dict
            dictionary of concentrations of elements obtained from  transport step
        """
        self.c_mob = copy(c_dict)
        for i in range(self.nx):
            T,P = self.T[i],self.P[i]
            b=[]
            for name in self.element_names:
                b.append(c_dict[name][i]+self.c_immob[name][i])
            self.gem.equilibrate(T,P,b)
            T,P,b = self.gem.temperature(),self.gem.pressure(),self.gem.elementAmounts()
            c_mob  = self.gem.elementAmountsInPhase(self.phase_names.index('aq_gen'))
            for j,name in enumerate(self.element_names):
                self.c_mob[name][i]=c_mob[j]
                self.c_immob[name][i]=b[j]-c_mob[j]
            pVol = self.gem.phaseVolumes()
            pAmt  = self.gem.phaseAmounts()
            V = self.gem.systemVolume()
            for j,name in enumerate(self.phase_names):
                self._phVfrac[name][i] = pVol[j]/V
                self._phConc[name][i] = pAmt[j]             
            self._poros[i]=self._phVfrac['aq_gen'][i]
            self.pH[i] = self.gem.pH()
            self.pE[i]=self.gem.pe()
        return 
 
    def get_ic(self):
        """
        provides element concentrations for initializing transport solver 
        
        Returns
        -------
        c_dict: dict
            dictionary of concentrations of elements  
        """
        out=[]
        for fname in self.gems_ic_files:
            T,P,b=self.read_lst_file(fname)
            self.gem.equilibrate(T, P, b)  
            T = self.gem.temperature() 
            P = self.gem.pressure()  
            b = self.gem.elementAmounts()  
            V = self.gem.systemVolume()
            c_mob  = self.gem.elementAmountsInPhase(self.phase_names.index('aq_gen'))
            ph,pe =self.gem.pH(),self.gem.pe() 
            pVol =self.gem.phaseVolumes()
            pAmount = self.gem.phaseAmounts()
            out.append([T,P,V,c_mob,b-c_mob,ph,pe,pVol,pAmount])
        c_dict={}
        self.c_immob ={}
        self.c_mob = {}
        for name in self.element_names:
            c_dict[name]=[]
            self.c_mob[name]=[]  
            self.c_immob[name]=[]
        self._phConc ={}
        self._phVfrac={}
        self._poros = []
        for name in self.phase_names:
            self._phConc[name]=[]
            self._phVfrac[name]=[]            
        for i in xrange(self.nx):
            idx = self.ic_cell_labels[i]
            T,P,V,c_mob,c_immob,ph,pe,phaseVol,phaseAmt = out[idx]
            self.T[i]=T
            self.P[i]=P
            for j,name in enumerate(self.element_names):
                self.c_immob[name].append(c_immob[j])
                self.c_mob[name].append(c_mob[j])
                c_dict[name].append(c_mob[j])  
            for j,name in enumerate(self.phase_names):
                self._phConc[name].append(phaseAmt[j])             
                self._phVfrac[name].append(phaseVol[j]/V)
                if name == 'aq_gen':
                    self._poros.append(phaseVol[j]/V)
            self.pH[i] = ph
            self.pE[i]=pe
        return c_dict
    
    def get_bc(self):
        """
        provides element concentration for 
        """
        out=[]
        idx = 0
        for fname in self.gems_bc_files:
            T,P,b=self.read_lst_file(fname)
            self.gem.equilibrate(T, P, b)
            T = self.gem.temperature() 
            P = self.gem.pressure() 
            b = self.gem.elementAmounts() 
            c_mob  = self.gem.elementAmountsInPhase(self.phase_names.index('aq_gen'))
            out.append({})
            for i,name in enumerate(self.element_names):
                out[idx][name]=c_mob[i]
            idx+=1
        return out

    def get_cdict(self):
        """
        returns element concentration for 
        """
        return self.c_mob

    @property
    def porosity(self):
        """
        porosity
        """
        return self._poros 
   
    @property
    def phase_volume_frac(self):
        """
        phase volume fractions
        """
        return self._phVfrac
    
    @property
    def phase_conc(self):
        """
        phase concentrations
        """
        return self._phConc
    
    @staticmethod
    def read_lst_file(fname):
        gem = ChemicalEngine(fname)
        return gem.temperature(),gem.pressure(),gem.elementAmounts()
