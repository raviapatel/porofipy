# -*- coding: utf-8 -*-
#=======================================================================================
#This File is part of FiPyRT
#=======================================================================================
#
#Copyright (C) 2016-2017  <Author> Ravi A. Patel <Email> ravee.a.patel@gmail.com
#
#This program is free software: you can redistribute it and/or modify it under the
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
#PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

#=======================================================================================
#%%
from __future__ import division,print_function
import sys,os
FipyRTPath = '..' 
sys.path.append(FipyRTPath)
import numpy as np
import matplotlib.pylab as plt
from FiPyRT import AdvectionDiffusion1D,Grid1D,GemsReactiveTransport1D
font = {'family' : 'serif',
         'size'   : 16}
plt.rc('font',**font)
#%%set domain
dx =0.5/80
m = Grid1D(0.5,0.5/80) 
#%%set up reactive transport model
#domain params
dp={}
dp['u']= (9.375e-6,)
dp['D0']=9.375e-6 * 0.0067
#dp['phiw']=0.32
dp['gems_init_file']=os.path.join('..','resources','CalciteIC-dat.lst')
dp['gems_ic_files']=[os.path.join('..','resources','CalciteIC-dat.lst')]
dp['ic_cell_labels'] = 0
#bc params
bc={}
bc['left']=['c',0]
bc['right']=['flux',0]
bc['boundary_solution_labels']={'left':0}
bc['gems_bc_files']=[os.path.join('..','resources','CalciteBC-dat.lst')]
#solver parameters
sp={'solutiontype':'crank'}
rt= GemsReactiveTransport1D(AdvectionDiffusion1D,m,dp,bc,sp)
#%%run model
De = 9.375e-6 * 0.0067*0.32  
dt = 0.1*dx**2/De

while rt.t < 21000:
    if m.rank == 0: print (rt.t)
    rt.advance(dt) # 
#%%   plot figure
cdict = rt.trans.cdict()
fig,ax1= plt.subplots()
x = m.x.value
outlist=['Ca','Mg','Cl']
lineformat = ['-k','-b','-r','-g','-c']
i=0
for name in outlist:
    ax1.plot(x,cdict[name],lineformat[i],label = name,linewidth=6)
    i+=1
ax1.set_ylabel('ionic concentration (M)')
ax1.set_xlabel('Distance (m)')
ax1.set_ylim([0,2.1e-3])
ax1.set_xlim([0,0.5])
ax2 = ax1.twinx()
outlist=['Calcite','Dolomite-dis']
for name in outlist:
    ax2.plot(x,rt.gem.phase_conc[name],lineformat[i],label = name,linewidth=6)
    i+=1
ax2.set_ylabel('mineral concentration (M)')
h1, l1 = ax1.get_legend_handles_labels()
h2, l2 = ax2.get_legend_handles_labels()
ax1.legend(h1+h2, l1+l2, loc=1,fontsize=24)   
ax2.set_ylim([0,4e-4])
ax2.set_xlim([0,0.5])
ax1.ticklabel_format(style='sci',axis='y',scilimits=(0,0),useMathText=True)
ax2.ticklabel_format(style='sci',axis='y',scilimits=(0,0),useMathText=True)
plt.show()       
