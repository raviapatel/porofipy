# -*- coding: utf-8 -*-
#=======================================================================================
#This File is part of FiPyRT
#=======================================================================================
#
#Copyright (C) 2016-2017  <Author> Ravi A. Patel <Email> ravee.a.patel@gmail.com
#
#This program is free software: you can redistribute it and/or modify it under the
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
#PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

#=======================================================================================
#%%
from __future__ import division,print_function
import sys
FipyRTPath = '..' 
sys.path.append(FipyRTPath)
from FiPyRT import AdvectionDiffusion1D,Grid1D
import numpy as np
from scipy.special import erfc
import matplotlib.pylab as plt
#%% some parameters
D = 1e-9
Lx = 20e-3
Pe = 0.4#grid peclet
u = (1e-6,)
dx = Pe * D/u[0]
dt = 0.3* dx**2/D
cb = 1
nx = Lx/dx
tf = 4000  
#%%analytical solution
def c_any():
    global cb,t,u
    ux=u[0]
    c =  cb * erfc(x/np.sqrt(4*D*t))   
    c = (cb/2.)*(erfc((x - ux*t)/(4*D*t)**0.5)+
         erfc((x+ ux * t)/(4*D *t)**0.5)*np.exp(ux*x/D))
    return c
#%%solution with our diffusion1D
m = Grid1D(Lx,dx)
ade = AdvectionDiffusion1D(m,dp={'phiw':1.,'tort':1.,'D0':D,'c':0.,'u':u},bc={'left':['c',cb]},
                  solverparams={'solutiontype':'explicit'})
while ade.t<=tf:
    if m.rank==0:   print ("current time: %s"%ade.t)
    ade.advance(dt)
t = ade.t
x=m.cellCenters.globalValue[0]
c = ade.c.globalValue
if m.rank==0:
    plt.figure()
    plt.plot(x*1e3,c,label='numerical')
    plt.plot(x*1e3,c_any(),'--',label='analytical')
    plt.xlabel('distance (cm)')
    plt.legend()
    plt.ylabel('c')
    plt.show()
