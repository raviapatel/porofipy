# -*- coding: utf-8 -*-
#=======================================================================================
#This File is part of FiPyRT
#=======================================================================================
#
#Copyright (C) 2016-2017  <Author> Ravi A. Patel <Email> ravee.a.patel@gmail.com
#
#This program is free software: you can redistribute it and/or modify it under the
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
#PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

#=======================================================================================
#%%
from __future__ import division,print_function
import sys
import os
PARENT = '..'
sys.path.append(os.path.join(os.path.dirname(__file__), PARENT))
FipyRTPath = '..' 
sys.path.append(FipyRTPath)
from FiPyRT import AdvectionDiffusion1D,Grid1D,PhrqcReactiveTransport1D
import time as timer
import numpy as np
import matplotlib.pylab as plt
#%% function to read comsol results
def read_comsol_results():
    res=np.loadtxt('../resources/benchmark7.csv',delimiter=',',skiprows=1)
    with open('../resources/benchmark7.csv','r') as f:
        head=f.readline()
        head=head.split(',')
        head = [name.rstrip() for name in head]
    comsol={}
    for i,name in enumerate(head):
        comsol[name]=res[:,i]
    return comsol
#%%set domain
dx =0.08/60
m = Grid1D(0.08,dx)
#%%set reactive transport model
#domain params
dp={}
D = (0.2/100)*(0.00027777/100)
dp['u']=(0.00027777/100,)
dp['D0']=(0.2/100)*(0.00027777/100)
dp['database']='phreeqc.dat'
dp['phrqc_input_file']='../resources/benchmark7.phrq'
dp['solution_labels']=100002
dp['tracer_components']=[u'Cl',u'N']
#bc params
bc={}
bc['left']=['cauchy',0]
bc['right']=['flux',0]
bc['boundary_solution_labels']={'left':100001}
#solver parameters
sp={'solutiontype':'crank'}
rt= PhrqcReactiveTransport1D(AdvectionDiffusion1D,m,dp,bc,sp)
dt = 0.5* dx**2/D
#%%run model
results={}
time=[]
t0=timer.time()
for name in rt.trans.components:
    if name not in ['H','O']:
        results[name]=[]
iters = 0
while rt.t <=(86400):
    iters += 1
    rt.advance(dt)
    if (iters%2==0):
        if m.rank==0: print("time: %s"%rt.t)
        time.append(rt.t)
        for name in rt.trans.components:
            if name not in ['H','O']:
                comp=results[name].append(rt.trans[name].c.globalValue[-1])
if m.rank==0:print('time taken: %s mins'%((timer.time()-t0)/60))
#%%plot_results
if m.rank==0:
    plt.figure()
    #plot lb results
    for name,v in results.iteritems():
        plt.plot(time,v,'--',label=name)
    #plot comsol_results
    comsol = read_comsol_results()
    for name,v in comsol.iteritems():
        if name!='time':
            plt.plot(comsol['time'],v,'-',markevery=10,label=name)
    plt.legend()
    plt.xlabel('time (s)')
    plt.ylabel('c (mol/lit)')
    plt.show()


