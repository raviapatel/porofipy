# -*- coding: utf-8 -*-
#=======================================================================================
#This File is part of FiPyRT
#=======================================================================================
#
#Copyright (C) 2016-2017  <Author> Ravi A. Patel <Email> ravee.a.patel@gmail.com
#
#This program is free software: you can redistribute it and/or modify it under the
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
#PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

#=======================================================================================
#%%
# -*- coding: utf-8 -*-
from __future__ import division,print_function
import sys
FipyRTPath = '..' 
sys.path.append(FipyRTPath)
from FiPyRT import Diffusion1D,Grid1D
import numpy as np
from scipy.special import erfc
import matplotlib.pylab as plt
#%% some parameters
D = 1e-9
Lx = 10e-3
nx = 50 
dx = Lx/nx
cb=1
dt = 5* dx**2/D
tf = 5000
#%%analytical solution
def c_any():
        global cb,x,D,t
        return  cb * erfc(x/np.sqrt(4*D*t)) 
#%%solution with our diffusion1D
m = Grid1D(Lx,dx)
ade = Diffusion1D(m,dp={'phiw':1.,'tort':1.,'D0':D,'c':0.},bc={'left':['c',cb]},
                  solverparams={'solutiontype':'crank'})
while ade.t <= tf:
    if m.rank==0:   print ("current time: %s"%ade.t)
    ade.advance(dt)
#get global values
x=m.cellCenters.globalValue[0]
c=ade.c.globalValue
t = ade.t
#plot only once
if m.rank==0:
    plt.figure()
    plt.plot(x*1e3,c,label='numerical')
    plt.plot(x*1e3,c_any(),'--',label='analytical')
    plt.xlabel('distance (cm)')
    plt.legend()
    plt.ylabel('c')
    plt.show()

