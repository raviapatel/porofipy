PoroFipy
======

PoroFipy is multiphysics framework developed on top of FiPy to simulate multiphyscis processes in porous media. At present PoroFiPy includes a reactive transport code coupling geochemical solvers [xGEMS](https://bitbucket.org/gems4/xgems/src/master/) and phreeqc via [IPhreeqcPy](https://raviapatel.bitbucket.io/IPhreeqcPy/index.html) with [Fipy](https://www.ctcms.nist.gov/fipy/)-a finite volume based pde solver.

Developer
=========
Ravi A. Patel
Karlsruhe Institue of Technology
ravee.a.patel@gmail.com

Installation
============
Running PoroFiPy requires following softwares to be installed. 

1. [Fipy](https://www.ctcms.nist.gov/fipy/)
2. [IPhreeqcPy](https://raviapatel.bitbucket.io/IPhreeqcPy/index.html)
3. [xGEMS](https://bitbucket.org/gems4/xgems/src/master/)

Each of this software has their installation instructions. PoroFiPy itself doesnot require any installation.

Documentation
=============
PoroFiPy is currently being developed and lacks detailed documentation


TERMS OF USE
============
This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or(at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details. You should have received a copy of the GNU Lesser General Public Licensealong with this program.  If not, see <https://www.gnu.org/licenses/>
